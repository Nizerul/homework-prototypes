//Создаем конструктор Student

function Student(name, point){
    this.name = name;
    this.point = point;
}

Student.prototype.show = function(){
	console.log('Студент %s набрал %s баллов', this.name, this.point);
}

//Создаем конструктор StudentList

/*Исправление: студенты заносятся непосредственно в объект*/

function StudentList(groupName, students){
	this.groupName = groupName;
  if(students !== undefined){
    for ( var i = 0, imax = students.length; i < imax; i += 2){
    	this.push(new Student(students[i], students[i+1]));
    }
  }
}

StudentList.prototype = Object.create(Array.prototype);

StudentList.prototype.add = function(name, point){
	this.push (new Student(name, point));
}

//Создаем список группы HJ-2

var hj2 = new StudentList('HJ-2', ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0]);

// Добавляем студентов в HJ-2

hj2.add('Семен Сенов', 20);
hj2.add('Кирилл Жуков', 40);
hj2.add('Иван Норлов', 10);

//Создаем группу HTML-7
var html7 = new StudentList('HTML-7',[]);

html7.add('Александр Хромов', 20);
html7.add('Леонид Дымовой', 30);
html7.add('Вячеслав Яровой', 50);

//Добавляем списка метод show

StudentList.prototype.show = function(){
  if(this.length === 1){
  	console.log('Группа %s (%d студент):', this.groupName, this.length);
  }else if(this.length <= 4){
  	console.log('Группа %s (%d студента):', this.groupName, this.length);
  }else{
  	console.log('Группа %s (%d студенов):', this.groupName, this.length);
  }
  this.forEach(function(student){
  	student.show();
  });
  console.log('');
}

//Выводим информацию о группах

hj2.show();
html7.show();

//Функция перевода студента

function studentTransfer(groupFrom, groupTo, name){
	groupFrom.forEach(function(student, i){
  	if(student.name === name){
    	groupTo.add(student.name, student.point);
      groupFrom.splice(i,1);
    }
  })
}

studentTransfer(hj2, html7, 'Семен Сенов');

//Дополнительное задание

StudentList.prototype.max = function(){
	var max = Math.max.apply(null, this.map(function(student){
  	return student.point.valueOf();
  }));
  
  return this.find(function(student){
  	return student.point === max;
  })
}

var bestStudent = html7.max();
bestStudent.show();